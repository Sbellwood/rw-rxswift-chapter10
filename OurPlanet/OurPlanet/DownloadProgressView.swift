//
//  DownloadProgressView.swift
//  OurPlanet
//
//  Created by Skyler Bellwood on 7/13/20.
//  Copyright © 2020 Ray Wenderlich. All rights reserved.
//

import UIKit

class DownloadProgressView: UIStackView {
  
  let label = UILabel()
  let progressView = UIProgressView()
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    translatesAutoresizingMaskIntoConstraints = false
    
    axis = .horizontal
    spacing = 0
    distribution = .fillEqually
    
    if let superview = superview {
      backgroundColor = UIColor.white
      bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
      leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
      rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
      heightAnchor.constraint(equalToConstant: 38).isActive = true
      
      label.text = "Downloads"
      label.translatesAutoresizingMaskIntoConstraints = false
      label.backgroundColor = .lightGray
      label.textAlignment = .center
      
      progressView.translatesAutoresizingMaskIntoConstraints = false
      
      let progressWrap = UIView()
      progressWrap.translatesAutoresizingMaskIntoConstraints = false
      progressWrap.backgroundColor = .lightGray
      progressWrap.addSubview(progressView)
      
      progressView.leftAnchor.constraint(equalTo: progressWrap.leftAnchor).isActive = true
      progressView.rightAnchor.constraint(equalTo: progressWrap.rightAnchor, constant: -10).isActive = true
      progressView.heightAnchor.constraint(equalToConstant: 4).isActive = true
      progressView.centerYAnchor.constraint(equalTo: progressWrap.centerYAnchor).isActive = true
      
      addArrangedSubview(label)
      addArrangedSubview(progressWrap)
    }
  }
}
